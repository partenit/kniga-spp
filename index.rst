Книга "Бизнес-модель СПП"
=========================

Содержание:

.. toctree::
   :maxdepth: 1

   vstuplenie
   biznes_model
   an_spp
   raspredelenie
   organizacia_raboty
   zakrytaja_otkrytaja_bazy
   spec_servisy
   objedinenija
